package es.cipfpbatoi.psp.u4.sync.inmorethanoneclass;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ContadorCaracteresTest {

    @Test
    void contarCaracteres()
    {
        String frase = "AAAaa";
        char[] caracteres = new char[]{'A','a'};

        Contador contador = new Contador();

        assertEquals(0,contador.getValorContador());

        Thread contarCaracteresT = new Thread(new ContadorCaracteres(caracteres, frase,contador));

        contarCaracteresT.start();

        try {
            contarCaracteresT.join();
        } catch (InterruptedException e) {
            fail(e);
        }

        assertEquals(frase.length(),contador.getValorContador());


    }

}