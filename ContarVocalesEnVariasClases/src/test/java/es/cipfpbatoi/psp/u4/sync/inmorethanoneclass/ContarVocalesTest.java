package es.cipfpbatoi.psp.u4.sync.inmorethanoneclass;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ContarVocalesTest {

    @ParameterizedTest
    @CsvSource(
            value = {"13;Aquesta es la millor classe de totes",
                    "10;En Cornellá donde las dan, las toman"},delimiter = ';'
    )
    void getTotalVocales(int vocales,String cadena) {

        ContarVocales contarVocales = new ContarVocales(cadena);
        contarVocales.crearYEsperarHilos();

        assertEquals(vocales,contarVocales.getTotalVocales());

    }
}