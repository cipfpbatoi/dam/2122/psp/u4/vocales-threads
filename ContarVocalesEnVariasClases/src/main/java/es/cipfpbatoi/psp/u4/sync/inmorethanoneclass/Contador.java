package es.cipfpbatoi.psp.u4.sync.inmorethanoneclass;

/**
 * Esta clase es el recurso compartido que se utiliza para almacenar
 * el contador actual
 *
 */
public class Contador {

    int contador;

    /**
     * Incializamos el contador a 0
     */
    public Contador(){
        this.contador = 0;
    }

    /**
     * Incrementa el contador en uno en modo sincronizado
     */
    public synchronized void incrementarContador()
    {
        contador++;
    }

    /**
     *
     * @return El valor actual de contador
     */
    public int getValorContador()
    {
        return contador;
    }

}
