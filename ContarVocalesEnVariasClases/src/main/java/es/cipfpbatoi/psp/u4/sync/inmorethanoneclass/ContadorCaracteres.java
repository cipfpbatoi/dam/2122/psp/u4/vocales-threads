package es.cipfpbatoi.psp.u4.sync.inmorethanoneclass;

/**
 * Esta clase cuenta todas las coincidencias del array de
 * caracteres que le hemos pasado con la string a la hora
 * de contruierla
 *
 */
public class ContadorCaracteres implements Runnable{

    char[] caracteres;
    String frase;
    Contador contador;

    /**
     * Array de Chars con la vocal que queramos contar,
     * tanto en mayúscula como minúscula
     *
     * @param caracteres Array de caracteres para buscar
     * @param frase String sobre el que se realizará la búsqueda
     * @param contador Parámetro de tipo contador donde se almacenará el resultado
     */
    ContadorCaracteres(char caracteres[], String frase, Contador contador){
        this.caracteres = caracteres;
        this.frase = frase;
        this.contador = contador;
    }

    private void vocalEncontrada()
    {
        contador.incrementarContador();
    }

    @Override
    public void run() {

        for (int i = 0; i<frase.length();i++){
            for (char c: caracteres) {
                if ( c == frase.charAt(i)){
                    vocalEncontrada();
                }
            }
        }
    }
}
