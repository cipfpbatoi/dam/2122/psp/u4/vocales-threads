package es.cipfpbatoi.psp.u4.sync.inmorethanoneclass;

/**
 * Esta clase cuenta las vocales de la string que le pasemos
 * al constuirla, el funcionamiento es:
 *
 * ContarVocales cv = new ContarVocales("Cadena de Texto");
 * cv.crearYEsperarHilos();
 * cv.getTotalVocales();
 */
public class ContarVocales {

    String frase;
    Contador contador;
    Thread[] contarVocalesT;
    private final String THREAD_PREFIX = "Thread_";


    /**
     * @param frase Frase sobre la que queremos contar las vocales
     */
    public ContarVocales(String frase)
    {
        this.frase = frase;
        this.contador = new Contador();
    }

    /**
     * Crea y espea los hilos que contarán el número de vocales.
     *
     */
    public void crearYEsperarHilos()
    {
        ContadorCaracteres contadorA = new ContadorCaracteres(new char[]{'a','A'},frase,contador);
        ContadorCaracteres contadorE = new ContadorCaracteres(new char[]{'e','E'},frase,contador);
        ContadorCaracteres contadorI = new ContadorCaracteres(new char[]{'i','I'},frase,contador);
        ContadorCaracteres contadorO = new ContadorCaracteres(new char[]{'o','O'},frase,contador);
        ContadorCaracteres contadorU = new ContadorCaracteres(new char[]{'u','U'},frase,contador);


        contarVocalesT = new Thread[] {
                new Thread( contadorA, THREAD_PREFIX+" A" ),
                new Thread( contadorE, THREAD_PREFIX+" E" ),
                new Thread( contadorI, THREAD_PREFIX+" I" ),
                new Thread( contadorO, THREAD_PREFIX+" O" ),
                new Thread( contadorU, THREAD_PREFIX+" U" )
        };

        for ( Thread t : contarVocalesT){
            t.start();
        }

        for ( Thread t : contarVocalesT){
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println(e.getLocalizedMessage());
            }
        }
    }

    /**
     * Devuelve el número total de vocales encontradas.
     *
     * @return Número total de vocales encontradas
     */
    public int getTotalVocales()
    {
        return contador.getValorContador();
    }

}
