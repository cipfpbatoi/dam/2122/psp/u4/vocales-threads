# Contar Vocales

Se ha implementado el ejercicio de clase de contar vocales con varios hilos utilizando dos formas:

1. Todo en una clase
2. Separado en varias clases.