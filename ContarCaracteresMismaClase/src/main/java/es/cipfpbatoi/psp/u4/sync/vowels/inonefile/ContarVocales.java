package es.cipfpbatoi.psp.u4.sync.vowels.inonefile;

public class ContarVocales {

    String frase;
    private int totalVocales = 0;

    final String THREAD_PREFIX = "THREAD_";

    /**
     * Este es el constructor por defecto al que le pasamos
     * una frase que queramos que nos cuente las vocales
     *
     * @param frase String con las vocales a contar
     */
    public ContarVocales(String frase)
    {
        this.frase = frase;
    }

    public int getTotalVocales() {
        return totalVocales;
    }

    /**
     * Esta clase se encarga de contar un tipo de vocal sobre la longitud de la cadena,
     *
     * Va llamando al método vocalEncontrada() cada vez que encuentra una vocal y
     * la actualiza de manera sincronizada
     */
    class ContadorCaracteres implements Runnable{

        char[] caracteres;

        /**
         * Array de Chars con los caracteres que queramos contr, tanto en mayúscula como minúscula.
         *
         * @param caracteres Caracteres a contar
         */
        ContadorCaracteres(char caracteres[]){
            this.caracteres = caracteres;
        }

        public synchronized void vocalEncontrada()
        {
            totalVocales++;
        }

        @Override
        public void run() {

            for (int i = 0; i<frase.length();i++){
                for (char c: caracteres) {
                    if ( c == frase.charAt(i)){
                        vocalEncontrada();
                    }
                }
            }
        }

    }

    public void crearYEsperarHilos(){

        ContadorCaracteres contadorA = new ContadorCaracteres(new char[]{'a','A'});
        ContadorCaracteres contadorE = new ContadorCaracteres(new char[]{'e','E'});
        ContadorCaracteres contadorI = new ContadorCaracteres(new char[]{'i','I'});
        ContadorCaracteres contadorO = new ContadorCaracteres(new char[]{'o','O'});
        ContadorCaracteres contadorU = new ContadorCaracteres(new char[]{'u','U'});


        Thread contaVocales[] = {
                new Thread( contadorA, THREAD_PREFIX+" A" ),
                new Thread( contadorE, THREAD_PREFIX+" E" ),
                new Thread( contadorI, THREAD_PREFIX+" I" ),
                new Thread( contadorO, THREAD_PREFIX+" O" ),
                new Thread( contadorU, THREAD_PREFIX+" U" )
        };

        for ( Thread t : contaVocales){
            t.start();
        }

        for ( Thread t : contaVocales){
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println(e.getLocalizedMessage());
            }
        }

    }

    public static void main(String[] args) {

        String str = "Aquesta es la millor classe de totes";
        ContarVocales contarVocales = new ContarVocales(str);
        contarVocales.crearYEsperarHilos();
        System.out.println("El resultado de contar las vocales es: "+contarVocales.totalVocales);

    }
}
